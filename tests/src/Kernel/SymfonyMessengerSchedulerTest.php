<?php

declare(strict_types = 1);

namespace Drupal\Tests\symfony_messenger_scheduler\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\symfony_messenger_scheduler_test\Messenger\ScheduledTestMessageHandler;
use Drupal\symfony_messenger_scheduler_test\ScheduledTestMessage;
use Symfony\Component\Clock\Clock;
use Symfony\Component\DependencyInjection\ServiceLocator;
use Symfony\Component\Messenger\RoutableMessageBus;

/**
 * Tests Symfony Messenger Scheduler.
 */
class SymfonyMessengerSchedulerTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'symfony_messenger_scheduler_test',
    'symfony_messenger_scheduler',
    'symfony_messenger',
  ];

  /**
   * Tests scheduler.
   *
   * Implements a simple version of \Symfony\Component\Messenger\Worker.
   *
   * @covers \Symfony\Component\Scheduler\Messenger\SchedulerTransport
   *
   * @see \Drupal\symfony_messenger_scheduler_test\Messenger\ScheduledTestMessageScheduleProvider
   * @see \Symfony\Component\Messenger\Worker
   */
  public function testSchedulerTransport(): void {
    $scheduleProviderId = 'scheduler_test';

    /** @var \Symfony\Component\Scheduler\Messenger\SchedulerTransport $schedulerTransport */
    $schedulerTransport = $this->receiverLocator()->get('scheduler_' . $scheduleProviderId);

    /** @var \Symfony\Component\Messenger\RoutableMessageBus $bus */
    $bus = \Drupal::service(RoutableMessageBus::class);

    $clock = new Clock();
    $end = $clock->now()->modify('+11 seconds');

    /** @var \Symfony\Component\Messenger\Envelope[] $all */
    $all = [];
    while ($end > $clock->now()) {
      $envelopes = $schedulerTransport->get();
      foreach ($envelopes as $envelope) {
        $all[] = $bus->dispatch($envelope);
      }

      $clock->sleep(0.250);
    }

    // The ScheduledTestMessageScheduleProvider schedule provider emits
    // messages every 2 seconds. Over a duration of 11 seconds, this results
    // in 5 messages.
    static::assertCount(5, $all);
    for ($i = 0; $i < 5; $i++) {
      $message = $all[$i]->getMessage();
      $this->assertInstanceOf(ScheduledTestMessage::class, $message);
      static::assertEquals(ScheduledTestMessageHandler::class . '::__invoke', $all[$i]->getMessage()->handledBy);
    }
  }

  /**
   * The receiver (transport) locator.
   */
  private function receiverLocator(): ServiceLocator {
    return \Drupal::service('messenger.receiver_locator');
  }

}
