<?php

declare(strict_types = 1);

namespace Drupal\symfony_messenger_scheduler_test;

/**
 * @see \Drupal\symfony_messenger_scheduler_test\Messenger\ScheduledTestMessageHandler
 */
final class ScheduledTestMessage {

  /**
   * Creates a new ScheduledTestMessage.
   */
  public function __construct(
    public ?string $handledBy = NULL,
  ) {
  }

}
