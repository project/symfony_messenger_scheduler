<?php

declare(strict_types = 1);

namespace Drupal\symfony_messenger_scheduler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Service provider for unprivatizing schedule providers.
 *
 * Workaround for https://www.drupal.org/project/drupal/issues/3391860
 */
final class SymfonyMessengerSchedulerUnprivatizeScheduleProviderCompilerPass implements CompilerPassInterface {

  /**
   * {@inheritdoc}
   */
  public function process(ContainerBuilder $container): void {
    $scheduleProviders = $container->findTaggedServiceIds('scheduler.schedule_provider');
    foreach (array_keys($scheduleProviders) as $serviceId) {
      $container
        ->getDefinition($serviceId)
        ->setPublic(TRUE);
    }
  }

}
