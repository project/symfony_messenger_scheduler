Symfony Messenger Scheduler

https://www.drupal.org/project/symfony_messenger_scheduler

**Key notes**

  - Message objects are only constructed once, then sent multiple times.
  - The consume transport command must be running in order for messages to be
    dispatched.
    - The transport must be running at the time message intervals are scheduled
    - The transport will not retroactively _catch-up_ with messages not
       dispatched during the time it was not running.
  - Transport command must be restarted for code changes to message and handlers
    to take effect.

# API

To make use of scheduler, you'll need:

1. Message class — See _Symfony Messenger_ README.md
2. Message handler — See _Symfony Messenger_ README.md
3. Schedule Provider — See below

## Implementing a Schedule Provider

A schedule provider is a class residing in the src/Messenger directory of a
module. The class requires:

1. Tagged with `#[\Symfony\Component\Scheduler\Attribute\AsSchedule]` attribute.
2. Implements `\Symfony\Component\Scheduler\ScheduleProviderInterface`
   interface:
   1. `getSchedule` method

Starter template:

```php
<?php

declare(strict_types = 1);

namespace Drupal\my_module\Messenger;

use Symfony\Component\Scheduler\Attribute\AsSchedule;
use Symfony\Component\Scheduler\ScheduleProviderInterface;

#[AsSchedule('my_scheduler_name')]
final class MyScheduleProvider implements ScheduleProviderInterface {

  /**
   * {@inheritdoc}
   */
  public function getSchedule(): Schedule {
    return (new Schedule())->add(
      RecurringMessage::every('5 minutes', new MyMessage()),
    );
  }

}
```

Replace `my_module` with a module name, `MyScheduleProvider` with the class
name, `MyMessage` with the message class name, and modify the interval to your
liking. The message and message handler do not have any special requirements
to be compatible with Scheduler. See reference links below for more ways to
configure.

For dependency injection, schedule providers have autowiring enabled.

There are more advanced ways to define storage providers, such as with
service.yml file, which is useful for explicit dependency injection.

Discovery is triggered with a cache clear (`drush cr`).

To start automatically creating messages at the defined interval, run the
standard consume command provided by Symfony Messenger, with `scheduler_NAME`
as the argument.

For example the above starter template has `my_scheduler_name` as its name:

`./bin/symfony-messenger messenger:consume scheduler_my_scheduler_name`

If you have more than one transport, you can chain them by priority:

`./bin/symfony-messenger messenger:consume doctrine scheduler_my_scheduler_name scheduler_another_scheduler_name`

# References

> Symfony Messenger allows you to trigger messages that should be sent on a
> predefined schedule. It reuses the Messenger concepts you're already familiar
> with.

  - [New in Symfony 6.3: Scheduler Component](https://symfony.com/blog/new-in-symfony-6-3-scheduler-component)
  - [https://symfony.com/components/Scheduler](Scheduler Component)
    Placeholder page and documentation (as of October 2023)
  - [Symfony 6.3 curated new features](https://symfony.com/blog/symfony-6-3-curated-new-features)
  - [Keynote: The Scheduler Component](https://live.symfony.com/account/replay/video/807)
    (Requires free login, French audio, English subtitles and slides)

# License

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
